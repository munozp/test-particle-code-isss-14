# Test-particle code ISSS-14

## File description
- This README.md file
- particle-basic-motion.ipynb : main code for particle basic motion (this file should be modified)
- magnetic-reconnection.ipynb : main code for studying particle acceleration in magnetic reconnection (this file should be modified)
- fields_mhd.h5: Input datafile from a MHD simulation
- environment.yml: Downloads and loads the required python packages for the code when Binder stars

## How to open Jupyter Hub
- Go to [https://notebooks.mpcdf.mpg.de/isss](https://notebooks.mpcdf.mpg.de/isss)
- *Sign up first* using your ISSS username and password
- Login using the same username and password

## How to run the code
- In the left panel, click "particle-basic-motion.ipynb" or "magnetic-reconnection.ipynb"
- Click Menu "Run", then "Run All Cells" (or click the "fast forward" symbol under the notebook tab)
- After modifying any cell, you can re-run it by clicking the "play" symbol under the notebook tab (or Shift+Enter)
- When you are done, please shutdown the notebook (Click Menu "File" and then "Shut Down")

## What to modify
### Basic particle motion
- Observe particle trajectories and visualization of their motion
- Investigate how various pushers impact the precision of the Larmor motion in the magnetic field
- Investigate various particle drifts
- How the size of time step dt influences the precision of particle position?
- Which of the particle pushers is the most precise?


### Magnetic-reconnection
- Observe particle trajectories and, to better visualize them, change initial and final time.
- Identify where the particles are accelerated, and correlate with electromagnetic field values.
- Which drift motions can be identified?
- Initial conditions:
  - Put all particles at the same initial position, varying only the velocities
  - Put particles distributed along the current sheet, with exactly the same velocity
  - Use protons instead of electrons

## Notes
- Plots can be zoomed in (using the square symbol on the left of each plot), if you use '%matplotlib widget' in the header of the notebook
- Please avoid to use more than 4 GB of memory
- If the memory consumption is too high, you can restart everything by clicking the menu "Kernel", then "Restart Kernel and Run All Cells..."


